﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper_Postgresql.DAL;
using Dapper_Postgresql.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Dapper_Postgresql.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var repository = new ProductRepository();
            var products = repository.GetProducts();
            var product = repository.GetProductById(new Guid("e07dc97a-a3ed-11e8-8a51-f3478bcf0ae2"));

            var newProduct = new Product
            {
                Id = Guid.NewGuid(),
                DateCreated = DateTime.UtcNow,
                Name = "new product",
                CategoryId = new Guid("9bbd6d22-a3ed-11e8-aaaa-d729ee02154b")
            };

            repository.InsertProduct(newProduct);


            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
