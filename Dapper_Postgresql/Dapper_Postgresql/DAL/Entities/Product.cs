﻿using System;

namespace Dapper_Postgresql.DAL.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }

        public Category Category { get; set; }
    }
}
