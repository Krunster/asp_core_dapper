﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper_Postgresql.DAL.Entities;
using Npgsql;

namespace Dapper_Postgresql.DAL
{
    public class ProductRepository
    {
        private readonly string connectionString = "User ID=asp_user;Password=Password123;Host=localhost;Port=5433;Database=TestDapper;Pooling=true;";

        public IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public IEnumerable<Product> GetProducts()
        {
            using (IDbConnection conn = Connection)
            {

                string sQuery = "SELECT * FROM \"Product\" prod INNER JOIN \"Category\" cat ON cat.\"Id\" = prod.\"CategoryId\"";

                conn.Open();

                var result = conn.Query<Product, Category, Product>(sQuery,
                    (product, category) =>
                    {
                        product.Category = category;
                        return product;
                    })
                    .Distinct()
                    .ToList();

                return result;
            }
        }

        public Product GetProductById(Guid Id)
        {
            using (IDbConnection conn = Connection)
            {
                string sQuery = $"SELECT * FROM \"Product\" prod INNER JOIN \"Category\" cat ON cat.\"Id\" = prod.\"CategoryId\" WHERE prod.\"Id\" = @Id";
                conn.Open();

                var result = conn.Query<Product, Category, Product>(sQuery,
                    (product, category) =>
                    {
                        product.Category = category;
                        return product;
                    },
                    param: new { Id = Id })
                    .Distinct()
                    .FirstOrDefault();

                return result;
            }
        }

        public void InsertProduct(Product product)
        {
            using (IDbConnection conn = Connection)
            {
                string sQuery = $"INSERT INTO \"Product\" (\"Id\", \"Name\", \"CategoryId\", \"DateCreated\") VALUES (@Id, @Name, @CategoryId, @DateCreated)";
                conn.Open();

                conn.Execute(sQuery, new { Id = product.Id, Name = product.Name, CategoryId = product.CategoryId, DateCreated = product.DateCreated });
            }
        }
    }
}
